import styles from './metamask-button.module.less';
import { ConnectIcon } from '@ninja-power/ui-icons';
import { Icon } from '@ninja-power/ui-components';
import { ConfigProvider, Button } from 'antd';
import { ButtonProps } from 'antd/lib';

export interface MetamaskButtonProps {
  connectButtonProps: ButtonProps;
  isConnected: boolean;
  isMetamaskInstalled: boolean;
}

export function MetamaskButton({
  connectButtonProps,
  isConnected,
  isMetamaskInstalled,
}: MetamaskButtonProps) {
  return (
    <ConfigProvider
      theme={{
        components: {
          Button: {
            controlHeight: 56,
            paddingContentHorizontal: 32,
            colorTextDisabled: '#000',
          },
        },
      }}
    >
      {isMetamaskInstalled ? (
        <Button
          className={styles['container']}
          disabled={isConnected}
          {...connectButtonProps}
        >
          {isConnected ? 'CONNECTED' : 'METAMASK'}{' '}
          <Icon icon={<ConnectIcon />} />
        </Button>
      ) : (
        <a
          href="https://metamask.io/download/"
          target={'_blank'}
          rel="noreferrer"
        >
          <Button className={styles['button']} type={'default'}>
            INSTALL METAMASK
          </Button>
        </a>
      )}
    </ConfigProvider>
  );
}

export default MetamaskButton;
