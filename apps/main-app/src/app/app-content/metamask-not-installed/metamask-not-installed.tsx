import { MetamaskIcon } from '@ninja-power/ui-icons';
import styles from './metamask-not-installed.module.less';
import { Button, Col, Modal, Row, Typography } from 'antd';

export const MetamaskNotInstalled = () => {
  return (
    <Modal open footer={<></>}>
      <div className={styles['container']}>
        <Row>
          <Typography.Title level={2}>
            Metamask is not installed
          </Typography.Title>
          <Typography.Paragraph>
            To continue using the website You should install Metamask extension
          </Typography.Paragraph>
        </Row>
        <Row justify={'center'}>
          <Col>
            <a
              href="https://metamask.io/download/"
              target={'_blank'}
              rel="noreferrer"
            >
              <Button
                className={styles['button']}
                type={'default'}
                icon={<MetamaskIcon className={styles['icon']} />}
              >
                <Typography.Text className={styles['button-text']}>
                  <b>Install</b>
                </Typography.Text>
              </Button>
            </a>
          </Col>
        </Row>
      </div>
    </Modal>
  );
};
