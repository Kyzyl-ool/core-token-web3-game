import { HTMLProps } from 'react';
import { Col, Row, Typography } from 'antd';
import { GameStops } from '@ninja-power/ui-components';
import styles from './left-banner.module.less';
import { Duration } from 'luxon';
import { currencyName } from '@ninja-power/config';

interface IProps extends HTMLProps<HTMLDivElement> {
  stopsInDuration: Duration;
  rateChangeInDuration: Duration;
  currentRateValue: number;
  gameId: number;
}

export const LeftBanner = ({
  className,
  stopsInDuration,
  rateChangeInDuration,
  currentRateValue,
  gameId,
}: IProps) => {
  return (
    <div className={className}>
      <GameStops
        className={styles['elapsed-time']}
        stopsInDuration={stopsInDuration}
        gameId={gameId}
      />
      <Row wrap={false}>
        <Col flex={'160px'}>
          <Typography.Text type={'secondary'}>
            Rate&nbsp;change&nbsp;in:
          </Typography.Text>
        </Col>
        <Col flex={'2'}>
          <Typography.Text>
            {rateChangeInDuration.toFormat('h')}h&nbsp;
            {+rateChangeInDuration.toFormat('m') % 60}min&nbsp;
            {+rateChangeInDuration.toFormat('s') % 60}s
          </Typography.Text>
        </Col>
      </Row>
      <Row wrap={false}>
        <Col flex={'160px'}>
          <Typography.Text type={'secondary'}>
            Current&nbsp;rate:
          </Typography.Text>
        </Col>
        <Col flex={'2'}>
          <Typography.Text>
            1&nbsp;{currencyName}&nbsp;=&nbsp;{currentRateValue}&nbsp;Points
          </Typography.Text>
        </Col>
      </Row>
    </div>
  );
};
