import styles from './loading.module.less';
import { Row, Spin } from 'antd';
import { NinjaPowerIcon } from '@ninja-power/ui-icons';

/* eslint-disable-next-line */
export interface LoadingProps {
  isLoaderHidden?: boolean;
  message?: string;
}

export function Loading({ isLoaderHidden, message }: LoadingProps) {
  return (
    <div className={styles['container']}>
      <Row>
        <NinjaPowerIcon />
      </Row>
      {!isLoaderHidden && (
        <Row>
          <Spin size={'large'} />
        </Row>
      )}
      {message && <Row>{message}</Row>}
    </div>
  );
}

export default Loading;
