import styles from './menu-pages.module.less';
import React, { useEffect, useMemo } from 'react';
import ReactDOM from 'react-dom';
import { Menu } from 'antd';
import { useLocation, useNavigate } from 'react-router-dom';

interface MenuItem {
  title: string;
  pathname: string;
  content: React.ReactNode;
  hidden?: boolean;
}

export interface MenuPagesProps {
  items: MenuItem[];
  overlayRef: React.RefObject<HTMLDivElement>;
  defaultPathname: string;
}

export function MenuPages({
  items,
  overlayRef,
  defaultPathname,
}: MenuPagesProps) {
  const { pathname, search } = useLocation();
  const navigate = useNavigate();

  const foundElement = useMemo(() => {
    return items.find(({ pathname: p, hidden }) => !hidden && p === pathname);
  }, [items, pathname]);

  useEffect(() => {
    if (!foundElement) {
      navigate({ pathname: defaultPathname, search });
    } else {
      navigate({ pathname, search });
    }
  }, [defaultPathname, foundElement, navigate, pathname, search]);

  return (
    <div className={styles['container']}>
      <Menu
        selectedKeys={[pathname]}
        items={items
          .filter(({ hidden }) => !hidden)
          .map(({ pathname, title }) => ({
            label: title,
            onClick: () => navigate({ pathname, search }),
            key: pathname,
          }))}
      />
      {overlayRef?.current &&
        foundElement &&
        ReactDOM.createPortal(foundElement.content, overlayRef.current)}
    </div>
  );
}

export default MenuPages;
