import { makeAutoObservable } from 'mobx';
import { InputStore } from '../component-store/input-store';
import { GameStore } from './game-store';

export class TeamStore {
  yourPoints = 0;
  points = 0;
  plusPoints = 0;
  yourBalance = 0;
  totalBalance = 0;
  pointsShare = 0;
  inputStore = new InputStore();

  constructor(readonly gameStore: GameStore) {
    makeAutoObservable(this);
  }

  reset = () => {
    this.yourBalance = 0;
    this.points = 0;
    this.plusPoints = 0;
    this.yourBalance = 0;
    this.totalBalance = 0;
    this.pointsShare = 0;
  };

  updatePlusPoints = () => {
    this.plusPoints = +this.inputStore.value * this.gameStore.currentRateValue;
  };

  addPoints = (additionalPoints: number) => {
    this.points += additionalPoints;
    this.updatePointsShare();
  };

  addBalance = (additionalCoinsBalance: number) => {
    this.totalBalance += additionalCoinsBalance;
  };

  addYourPoints = (additionalYourPoints: number) => {
    this.yourPoints += additionalYourPoints;
    this.updatePointsShare();
  };

  updatePointsShare = () => {
    this.pointsShare = this.yourPoints / this.points;
  };
}
