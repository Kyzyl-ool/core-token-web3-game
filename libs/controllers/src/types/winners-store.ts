export interface WinnersItem {
  white: number;
  black: number;
  number: number;
  prize: number;
  whiteTokens: number;
  blackTokens: number;
}
