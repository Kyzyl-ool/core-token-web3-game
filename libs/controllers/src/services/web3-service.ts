declare let window: any;

import { ethers } from 'ethers';
import { JsonRpcSigner } from '@ethersproject/providers/src.ts/json-rpc-provider';
import { Contracts } from '@tug-of-war/contracts/dist/src/types';
import { getContractsForChainOrThrow } from '@tug-of-war/contracts';
import { ChainService } from './chain-service';
import { CHAIN_IDS, CHAIN_NAMES, CHAIN_RPC_URLS } from '../types/constants';

const jrpcUrl = CHAIN_RPC_URLS[ChainService.allowedChain];
const jrpcNetwork = {
  chainId: CHAIN_IDS[ChainService.allowedChain],
  name: CHAIN_NAMES[ChainService.allowedChain],
};

export class Web3Service {
  static jrpcProvider = new ethers.providers.JsonRpcProvider(
    jrpcUrl,
    jrpcNetwork,
  );
  static jrpcBatchProvider = new ethers.providers.JsonRpcBatchProvider(
    jrpcUrl,
    jrpcNetwork,
  );
  static provider = window.ethereum
    ? new ethers.providers.Web3Provider(window.ethereum)
    : Web3Service.jrpcProvider;
  static signer: JsonRpcSigner | null = null;
  static contracts: Contracts | null = null;

  public static async fetchSigner(accounts: string[]) {
    if (!Web3Service.provider) {
      throw new Error('No provider');
    }
    const changed = await ChainService.fixCurrentChain();
    Web3Service.signer = await Web3Service.provider.getSigner(accounts[0]);

    return [Web3Service.signer, changed];
  }

  static fetchContract(chainId: number) {
    Web3Service.contracts = getContractsForChainOrThrow(
      chainId,
      window.ethereum ? Web3Service.signer : Web3Service.provider,
    );

    return Web3Service.contracts;
  }

  static async initMetamask() {
    if (!Web3Service.provider) {
      throw new Error('No provider');
    }

    return await Web3Service.provider.listAccounts();
  }
}
