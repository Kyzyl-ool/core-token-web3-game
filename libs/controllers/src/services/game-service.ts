import { getCurrentGame } from '@tug-of-war/contracts';
import { Web3Service } from './web3-service';

export class GameService {
  static async getCurrentGame() {
    if (!Web3Service.contracts) {
      throw new Error('Web3Service.contracts is empty');
    }
    return getCurrentGame(Web3Service.contracts.gameFactoryContract);
  }
}
